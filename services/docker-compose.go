package services

import (
	"fmt"
	"log"

	"github.com/urfave/cli/v2"
)

var (
	tail    int
	service string
)

type DockerComposeService struct {
	*Service
}

func NewDockerComposeService(app *cli.App) (*DockerComposeService, error) {
	command := "docker-compose"
	if !commandExists(command) {
		command = "docker"
		if !commandExists(command) {
			return nil, fmt.Errorf("%s not found in $PATH", command)
		}
	}

	return &DockerComposeService{NewService(app, command)}, nil
}

func (c *DockerComposeService) isV1() bool {
	return c.cmd == "docker-compose"
}

func (c *DockerComposeService) send(args []string) error {
	if c.isV1() {
		log.Printf("### using docker-compose v1 ###")
		c.args = args
		return sendCommand(c)
	} else {
		log.Printf("### using docker compose v2 ###")
		c.args = append([]string{"compose"}, args...)
		return sendCommand(c)
	}
}

func (c *DockerComposeService) Command() string {
	return c.cmd
}

func (c *DockerComposeService) Commands() []*cli.Command {
	return []*cli.Command{
		c.Up(),
		c.Down(),
		c.Build(),
		c.Restart(),
		c.Rebuild(),
		c.Exec(service),
		c.Logs(service, tail),
		c.Status(),
		c.Services(),
	}
}

func (c *DockerComposeService) Up() *cli.Command {
	return &cli.Command{
		Name:  "up",
		Usage: fmt.Sprintf("%s up -d", c.cmd),
		Action: func(ctx *cli.Context) error {
			return c.send([]string{"up", "-d"})
		},
	}
}

func (c *DockerComposeService) Down() *cli.Command {
	return &cli.Command{
		Name:    "down",
		Aliases: []string{"dn"},
		Usage:   fmt.Sprintf("%s down --remove-orphans", c.cmd),
		Action: func(ctx *cli.Context) error {
			return c.send([]string{"down", "--remove-orphans"})
		},
	}
}

func (c *DockerComposeService) Build() *cli.Command {
	return &cli.Command{
		Name:    "build",
		Aliases: []string{"b"},
		Usage:   fmt.Sprintf("%s build --force-rm --progress=auto", c.cmd),
		Action: func(ctx *cli.Context) error {
			return c.send([]string{"build", "--force-rm", "--progress", "auto"})
		},
	}
}

func (c *DockerComposeService) Services() *cli.Command {
	return &cli.Command{
		Name:    "list",
		Aliases: []string{"ls"},
		Usage:   fmt.Sprintf("%s ps --services", c.cmd),
		Action: func(ctx *cli.Context) error {
			return c.send([]string{"ps", "--services"})
		},
	}
}

func (c *DockerComposeService) Status() *cli.Command {
	return &cli.Command{
		Name:    "status",
		Aliases: []string{"s"},
		Usage:   fmt.Sprintf("%s ps", c.cmd),
		Action: func(ctx *cli.Context) error {
			return c.send([]string{"ps"})
		},
	}
}

func (c *DockerComposeService) Exec(service string) *cli.Command {
	return &cli.Command{
		Name:    "exec",
		Aliases: []string{"x"},
		Usage:   fmt.Sprintf("%s exec [service] sh", c.cmd),
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "service",
				Usage:       "Service to open a shell into",
				Aliases:     []string{"s"},
				Destination: &service,
				Required:    true,
			},
		},
		Action: func(ctx *cli.Context) error {
			return c.send([]string{"exec", service, "sh"})
		},
	}
}

func (c *DockerComposeService) Logs(service string, tail int) *cli.Command {
	return &cli.Command{
		Name:    "logs",
		Aliases: []string{"l"},
		Usage:   fmt.Sprintf("%s logs -f --tail all [service]", c.cmd),
		Flags: []cli.Flag{
			&cli.IntFlag{
				Name:        "tail",
				Aliases:     []string{"t"},
				Destination: &tail,
			},
			&cli.StringFlag{
				Name:        "service",
				Usage:       "Service to log",
				Aliases:     []string{"s"},
				Destination: &service,
				Required:    true,
			},
		},
		Action: func(ctx *cli.Context) error {
			if tail > 0 {
				return c.send([]string{"logs", "-f", "--tail", fmt.Sprintf("%d", tail), service})
			}
			return c.send([]string{"logs", "-f", service})
		},
	}
}

func (c *DockerComposeService) Restart() *cli.Command {
	return &cli.Command{
		Name:    "restart",
		Aliases: []string{"rs"},
		Usage:   fmt.Sprintf("%s down && %s up -d", c.cmd, c.cmd),
		Action: func(ctx *cli.Context) error {
			if err := c.send([]string{"down", "--remove-orphans"}); err != nil {
				return err
			}
			return c.send([]string{"up", "-d"})
		},
	}
}

func (c *DockerComposeService) Rebuild() *cli.Command {
	return &cli.Command{
		Name:    "rebuild",
		Aliases: []string{"rb"},
		Usage:   fmt.Sprintf("%s down && %s build && %s up -d", c.cmd, c.cmd, c.cmd),
		Action: func(ctx *cli.Context) error {
			if err := c.send([]string{"down", "--remove-orphans"}); err != nil {
				return err
			}
			if err := c.send([]string{"build", "--force-rm", "--progress", "auto"}); err != nil {
				return err
			}
			return c.send([]string{"up", "-d"})
		},
	}
}
