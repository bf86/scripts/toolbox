package services

import (
	"fmt"

	"github.com/urfave/cli/v2"
)

var image, endpoint string

type DockerService struct {
	*Service
}

func NewDockerService(app *cli.App) (*DockerService, error) {
	command := "docker"
	if !commandExists(command) {
		return nil, fmt.Errorf("%s not found in $PATH", command)

	}
	return &DockerService{NewService(app, command)}, nil
}

func (c *DockerService) Commands() []*cli.Command {
	return []*cli.Command{
		c.Run(endpoint),
		c.Clean(),
	}
}

func (c *DockerService) Run(endpoint string) *cli.Command {
	return &cli.Command{
		Name:    "run",
		Aliases: []string{"r"},
		Usage:   "docker run --rm -it [image]",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "endpoint",
				Usage:       "Local endpoint to hit (if required)",
				Aliases:     []string{"e"},
				Destination: &endpoint,
				Required:    false,
			},
		},
		Action: func(ctx *cli.Context) error {
			c.args = []string{"run", "--rm", "-it"}
			c.args = append(c.args, ctx.Args().Slice()...)

			if endpoint != "" {
				c.args = append(c.args, "--endpoint", endpoint)
			}
			return sendCommand(c)
		},
	}
}

func (c *DockerService) Clean() *cli.Command {
	return &cli.Command{
		Name:    "clean",
		Aliases: []string{"c"},
		Usage:   "docker system prune && docker volume prune -f",
		Action: func(ctx *cli.Context) error {
			c.args = []string{"system", "prune"}
			if err := sendCommand(c); err != nil {
				return err
			}
			c.args = []string{"volume", "prune", "-f"}
			return sendCommand(c)
		},
	}
}
