# dev

Development shorthand scripts

```
NAME:
   dev - development tools

USAGE:
   dev [global options] command [command options] [arguments...]

VERSION:
   v0.0.1

AUTHOR:
   James King <jameshdking@gmail.com>

COMMANDS:
   up           docker-compose up -d
   down         docker-compose down --remove-orphans
   build, b     docker-compose build --force-rm --progress=auto
   restart, rs  docker-compose down && docker-compose up -d
   rebuild, rb  docker-compose down && docker-compose build && docker-compose up -d
   exec, x      docker-compose exec [service] bash
   logs, l      docker-compose logs -f --tail all [service]
   list, ls     docker-compose ps --services
   status, s    docker-compose ps
   clean, c     docker system prune && docker volume prune -f
   nuke         docker-compose down && docker system prune && docker volume prune -f && docker-compose build && docker-compose up -d
   help, h      Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --help, -h     show help (default: false)
   --version, -v  display the current tool version (default: false)
```
